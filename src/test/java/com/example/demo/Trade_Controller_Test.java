package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.bson.types.ObjectId;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.model.Trade;
import com.example.demo.model.TradeState;
import com.example.demo.model.TradeType;
import com.example.demo.repository.Trade_Repository;

import yahoofinance.Stock;
import yahoofinance.YahooFinance;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class Trade_Controller_Test {
	@Autowired
	Trade_Repository repository;
	@Autowired
	Trade_Controller c;

	@InjectMocks
	private Trade_Controller userController;
	
	@Mock
	private Trade_Repository userRepository;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void testCountGetAllTrades() {
		List<Trade> list = repository.findAll();
		assertEquals(91, list.size());
	}

	@Test
	void testGetTradeById() {
		Trade u = new Trade();
		u.set_id(new ObjectId("5f8022c519173f2f09dacf00"));
		when(userRepository.findBy_id(new ObjectId("5f8022c519173f2f09dacf00"))).thenReturn(u);

		Trade user = userController.getTradeById(new ObjectId("5f8022c519173f2f09dacf00"));

		verify(userRepository).findBy_id(new ObjectId("5f8022c519173f2f09dacf00"));

		assertEquals("5f8022c519173f2f09dacf00", user.get_id());
	}

	@Test
	void testModifyTradeById()
	{
		Trade trade3 = new Trade(new ObjectId(), new Date(), TradeState.State.CREATED, TradeType.Type.BUY, "C", 10, 44.5);
		
	    trade3.setState(TradeState.State.FILLED);
		userController.modifyTradeById(new ObjectId("5f8022c519173f2f09dacf00"), trade3);
		when(userRepository.findBy_id(new ObjectId("5f8022c519173f2f09dacf00"))).thenReturn(trade3);
		assertEquals(trade3.getState(),TradeState.State.FILLED);
		
	}
	@Test
	void testDeleteTradeById() {
		Trade u = new Trade();
		u.set_id(new ObjectId("5f8022c519173f2f09dacf00"));
		when(userRepository.findBy_id(new ObjectId("5f8022c519173f2f09dacf00"))).thenReturn(u);
		userController.deleteTrade(new ObjectId("5f8022c519173f2f09dacf00"));
		verify(userRepository, times(1)).delete(userRepository.findBy_id(new ObjectId("5f8022c519173f2f09dacf00")));
	}

	@Test
	void testCreateTradeStatusForbidden() throws Exception {
		Trade u = new Trade(new ObjectId(), new Date(), TradeState.State.CREATED, TradeType.Type.BUY, "C", 10, 44.5);
		ResponseEntity<Object> entity = userController.createTrade(u);
		HttpStatus statusCode = entity.getStatusCode();
		assertEquals(HttpStatus.FORBIDDEN, statusCode);
	}

	@Test
	void testCreateTradeStatusOk() throws Exception {
		Trade u = new Trade(new ObjectId(), new Date(), TradeState.State.CREATED, TradeType.Type.BUY, "HSBC", 10, 44.5);
		ResponseEntity<Object> entity = userController.createTrade(u);
		HttpStatus statusCode = entity.getStatusCode();
		assertEquals(HttpStatus.OK, statusCode);
	}

	@Test
	void testUpdateStatusok1() {
		Trade u = new Trade();
		u.set_id(new ObjectId("5f8022c519173f2f09dacf00"));
		String status = "FILLED";
		when(userRepository.findBy_id(new ObjectId("5f8022c519173f2f09dacf00"))).thenReturn(u);
		u.setState(TradeState.State.CREATED);
		if (u.getState().equals(TradeState.State.CREATED)) {
			ResponseEntity<Object> entity = userController.updateStatus(new ObjectId("5f8022c519173f2f09dacf00"),
					status);
			HttpStatus statusCode = entity.getStatusCode();
			assertEquals(HttpStatus.OK, statusCode);
		}
	}

	@Test
	void testUpdateStatusok2() {
		Trade u = new Trade();
		u.set_id(new ObjectId("5f8022c519173f2f09dacf00"));
		String status = "FILLED";
		when(userRepository.findBy_id(new ObjectId("5f8022c519173f2f09dacf00"))).thenReturn(u);
		u.setState(TradeState.State.PROCESSING);
		if (u.getState().equals(TradeState.State.PROCESSING)) {
			if (!status.equals("CREATED")) {

				ResponseEntity<Object> entity = userController.updateStatus(new ObjectId("5f8022c519173f2f09dacf00"),
						status);
				HttpStatus statusCode = entity.getStatusCode();
				assertEquals(HttpStatus.OK, statusCode);
			}
		}
	}

	@Test
	void testUpdateStatusForbidden1() {
		Trade u = new Trade();
		u.set_id(new ObjectId("5f8022c519173f2f09dacf00"));
		String status = "CREATED";
		when(userRepository.findBy_id(new ObjectId("5f8022c519173f2f09dacf00"))).thenReturn(u);
		u.setState(TradeState.State.PROCESSING);
		if (u.getState().equals(TradeState.State.PROCESSING)) {
			if (status.equals("CREATED")) {

				ResponseEntity<Object> entity = userController.updateStatus(new ObjectId("5f8022c519173f2f09dacf00"),
						status);
				HttpStatus statusCode = entity.getStatusCode();
				assertEquals(HttpStatus.FORBIDDEN, statusCode);
			}
		}
	}

	@Test
	void testUpdateStatusok3() {
		Trade u = new Trade();
		u.set_id(new ObjectId("5f8022c519173f2f09dacf00"));
		String status = "FILLED";
		when(userRepository.findBy_id(new ObjectId("5f8022c519173f2f09dacf00"))).thenReturn(u);
		u.setState(TradeState.State.FILLED);
		if (u.getState().equals(TradeState.State.FILLED)) {
			if (!(status.equals("CREATED") || status.equals("PROCESSING") || status.equals("FILLED"))) {

				ResponseEntity<Object> entity = userController.updateStatus(new ObjectId("5f8022c519173f2f09dacf00"),
						status);
				HttpStatus statusCode = entity.getStatusCode();
				assertEquals(HttpStatus.OK, statusCode);
			}
		}
	}

	@Test
	void testUpdateStatusForbidden2() {
		Trade u = new Trade();
		u.set_id(new ObjectId("5f8022c519173f2f09dacf00"));
		String status = "PENDING";
		when(userRepository.findBy_id(new ObjectId("5f8022c519173f2f09dacf00"))).thenReturn(u);
		u.setState(TradeState.State.FILLED);
		if (u.getState().equals(TradeState.State.FILLED)) {
			if ((status.equals("CREATED") || status.equals("PROCESSING") || status.equals("FILLED"))) {

				ResponseEntity<Object> entity = userController.updateStatus(new ObjectId("5f8022c519173f2f09dacf00"),
						status);
				HttpStatus statusCode = entity.getStatusCode();
				assertEquals(HttpStatus.FORBIDDEN, statusCode);
			}
		}
	}

	@Test
	void testUpdateStatusForbidden3() {
		Trade u = new Trade();
		u.set_id(new ObjectId("5f8022c519173f2f09dacf00"));
		String status = "REJECTED";
		when(userRepository.findBy_id(new ObjectId("5f8022c519173f2f09dacf00"))).thenReturn(u);
		u.setState(TradeState.State.FILLED);
		if (!((u.getState().equals(TradeState.State.CREATED)) || (u.getState().equals(TradeState.State.PROCESSING))
				|| (u.getState().equals(TradeState.State.FILLED)))) {

			ResponseEntity<Object> entity = userController.updateStatus(new ObjectId("5f8022c519173f2f09dacf00"),
					status);
			HttpStatus statusCode = entity.getStatusCode();
			assertEquals(HttpStatus.FORBIDDEN, statusCode);
		}
	}
	
	@Test
	void testGetTradeAdvice_sell() {
		Trade u = new Trade();
		when(userRepository.findBy_id(new ObjectId("5f8022c519173f2f09dacf00"))).thenReturn(u);
		double currentprice = u.getPrice();
		double currentprice_1 = currentprice - 1;
		double previousweek_price = 10;
		double no0_1 = currentprice * 0.001;
		double current_price0_1_high = currentprice + currentprice * 0.001;
		double current_price0_1_less = currentprice - currentprice * 0.001;
		double sum = currentprice;
		String expectedReturn = "SELL";
		String actualReturn;
		if(sum >= current_price0_1_less && sum <=current_price0_1_high) {
			actualReturn = "SELL";
		}
		else if((currentprice > currentprice_1) && (sum > previousweek_price)) {
			actualReturn = "BUY";
		}
		else  {
			actualReturn = "HOLD";
		}
		assertEquals(expectedReturn, actualReturn);
	}

	@Test
	void testGetTradeAdvice_buy() {
		Trade u = new Trade();
		when(userRepository.findBy_id(new ObjectId("5f8022c519173f2f09dacf00"))).thenReturn(u);
		double currentprice = u.getPrice();
		double currentprice_1 = currentprice - 1;
		double previousweek_price = 50;
		double no0_1 = currentprice * 0.001;
		double current_price0_1_high = currentprice + currentprice * 0.001;
		double current_price0_1_less = currentprice - currentprice * 0.001;
		double sum = 55;
		String expectedReturn = "BUY";
		String actualReturn;
		if(sum >= current_price0_1_less && sum <=current_price0_1_high) {
			actualReturn = "SELL";
		}
		else if((currentprice > currentprice_1) && (sum > previousweek_price)) {
			actualReturn = "BUY";
		}
		else  {
			actualReturn = "HOLD";
		}
		assertEquals(expectedReturn, actualReturn);
	}

	@Test
	void testGetTradeAdvice_hold() {
		Trade u = new Trade();
		when(userRepository.findBy_id(new ObjectId("5f8022c519173f2f09dacf00"))).thenReturn(u);
		double currentprice = u.getPrice();
		double currentprice_1 = currentprice + 1;
		double previousweek_price = 55;
		double no0_1 = currentprice * 0.001;
		double current_price0_1_high = currentprice + currentprice * 0.001;
		double current_price0_1_less = currentprice - currentprice * 0.001;
		double sum = 50;
		String expectedReturn = "HOLD";
		String actualReturn;
		if(sum >= current_price0_1_less && sum <=current_price0_1_high) {
			actualReturn = "SELL";
		}
		else if((currentprice > currentprice_1) && (sum > previousweek_price)) {
			actualReturn = "BUY";
		}
		else  {
			actualReturn = "HOLD";
		}
		assertEquals(expectedReturn, actualReturn);
	}

}
