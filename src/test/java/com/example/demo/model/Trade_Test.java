package com.example.demo.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.example.demo.model.TradeState.State;
import com.example.demo.model.TradeType.Type;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class Trade_Test {
@InjectMocks
Trade t;
@InjectMocks
TradeState state;
@InjectMocks
TradeType type;

public enum st {
   CREATED,
   PROCESSING,
   FILLED,
   REJECTED;
};
@Test
void getId_Test() {
	ObjectId _id = new ObjectId();
	String id = _id.toHexString();
	t.set_id(_id);
	assertEquals(t.get_id(), id);
}
@Test
 void testGetCreated(){
String n= new String("SBIN");
t.setTicker(n);
   assertThat(t.getTicker()).isEqualTo(n);
 }
@Test
 void testGetQuantity(){
int n= 5;
t.setQuantity(n);
   assertThat(t.getQuantity()).isEqualTo(n);
 }
@Test
 void testGetPrice(){
double n= 34.56;
t.setPrice(n);
   assertThat(t.getPrice()).isEqualTo(n);
 }
@Test
 void testGetDate(){
Date d= new Date();
t.setCreated(d);
   assertThat(t.getCreated()).isEqualTo(d);
 }
@Test
void testGetState()
{

State s= TradeState.State.CREATED;
t.setState(s);
assertThat(t.getState()).isEqualTo(s);

}
@Test
void testGetType()
{
Type s= TradeType.Type.BUY;
t.setType(s);
assertThat(t.getType()).isEqualTo(s);

}


}