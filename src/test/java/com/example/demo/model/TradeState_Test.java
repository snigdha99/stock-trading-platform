package com.example.demo.model;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.example.demo.model.TradeState.State;
import com.example.demo.model.TradeType.Type;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TradeState_Test {
@InjectMocks
Trade t;
@InjectMocks
TradeState state;
@InjectMocks
TradeType type;

@Test
 void testGetStatus(){
state.setTradeStatus(TradeState.State.CREATED);
   assertThat(state.getTradeStatus()).isEqualTo(TradeState.State.CREATED);
 }
}