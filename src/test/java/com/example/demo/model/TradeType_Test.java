package com.example.demo.model;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.example.demo.model.TradeState.State;
import com.example.demo.model.TradeType.Type;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TradeType_Test {
@InjectMocks
Trade t;
@InjectMocks
TradeState state;
@InjectMocks
TradeType type;

@Test
 void testGetType(){
type.setTradeType(TradeType.Type.BUY);
   assertThat(type.getTradeType()).isEqualTo(TradeType.Type.BUY);
 }
}