package com.example.demo;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Trade;
import com.example.demo.model.TradeState;
import com.example.demo.model.TradeType;
import com.example.demo.model.TradeType.Type;
import com.example.demo.repository.Trade_Repository;

import yahoofinance.Stock;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.histquotes.Interval;

@RestController
@CrossOrigin
@RequestMapping("/trades")

public class Trade_Controller {
	@Autowired
	private Trade_Repository repository;
	
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Trade> getAllTrades() {
	  return repository.findAll();
	}
	
	@RequestMapping(value = "/addTrades", method = RequestMethod.GET)
	public void addTrades() throws IOException {	
	 String[] symbol = new String[] {"AIG", "U11.SI", "G07.SI"};
	 double amount [] = new double[3];
	 for (int i=0; i<symbol.length; i++) {
	  amount[i] = YahooFinance.get(symbol[i]).getQuote().getPrice().doubleValue();
	  }
	 // double amount = YahooFinance.get("BAC").getQuote().getPrice().doubleValue();
	  Trade trade1 = new Trade(new ObjectId(), new Date(), TradeState.State.CREATED, TradeType.Type.BUY, symbol[0], 50, amount[0]);
	  repository.save(trade1);
	  Trade trade2 = new Trade(new ObjectId(), new Date(), TradeState.State.CREATED, TradeType.Type.BUY, symbol[1], 2, amount[1]);
	  repository.save(trade2);
	  Trade trade3 = new Trade(new ObjectId(), new Date(), TradeState.State.CREATED, TradeType.Type.BUY, symbol[2], 10, amount[2]);
	  repository.save(trade3);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Trade getTradeById(@PathVariable("id") ObjectId id) {
	  return repository.findBy_id(id);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public void modifyTradeById(@PathVariable("id") ObjectId id, @Valid @RequestBody Trade trade) {
	  trade.set_id(id);
	  repository.save(trade);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ResponseEntity<Object> createTrade(@Valid @RequestBody Trade trade) throws IOException {
		trade.set_id(ObjectId.get());
		trade.setCreated(new Date());
		trade.setState(TradeState.State.CREATED);
		trade.setType(TradeType.Type.BUY);
	    String[] symbols = new String[] {"INTC", "BABA", "TSLA", "AIR.PA", "HSBC", "JPM", "WFC", "BAC", "SAN", "RC", "GS", "LYG", "USB", "UBS", "MS", "AXP", "S23.SI", "HSBC", "D05.SI"};
	    double price [] = new double[symbols.length];
	    int flag = 50;
	    for (int i=0; i<symbols.length; i++) {
	       price[i] = YahooFinance.get(symbols[i]).getQuote().getPrice().doubleValue();
	  	    }
	    for(int j=0;j<symbols.length; j++) { 
		    if(trade.getTicker().equals(symbols[j])) {
		    	trade.setPrice(price[j]);
			    flag = 1;
			    repository.save(trade);
			    break;
		    }
		    else {
			    flag = 0;
		    }
	    }
	    if (flag == 0) {
		    System.out.println("Invalid Ticker");
		    return new ResponseEntity<>("Invalid Ticker!! Trade not created", HttpStatus.FORBIDDEN);
	    }
	    else {
	    	return new ResponseEntity<>("Trade created successfully !!", HttpStatus.OK);
	    }
	  }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void deleteTrade(@PathVariable ObjectId id) {
	  repository.delete(repository.findBy_id(id));
	}
	
	@RequestMapping(value = "/{id}/updateStatus", method = RequestMethod.PUT)
	public ResponseEntity<Object> updateStatus(@PathVariable ObjectId id, @RequestParam String status) {
	   Trade trade = repository.findBy_id(id);
	   if(trade.getState().equals(TradeState.State.CREATED))
	   {
		   trade.setState(TradeState.State.valueOf(status));
		   repository.save(trade);
		   return new ResponseEntity<>("Trade status updated successfully", HttpStatus.OK);
	   }
	   else if(trade.getState().equals(TradeState.State.PROCESSING))
	   {
		   if(!status.equals("CREATED"))
		   {
			   trade.setState(TradeState.State.valueOf(status));
			   repository.save(trade);
			   return new ResponseEntity<>("Trade status updated successfully", HttpStatus.OK);
		   }
		   else
		   {
			   return new ResponseEntity<>("Invalid operation", HttpStatus.FORBIDDEN);
		   }
	   }
	   else if(trade.getState().equals(TradeState.State.FILLED))
	   {
		   if(!(status.equals("CREATED") || status.equals("PROCESSING") || status.equals("FILLED")))
		   {
			   trade.setState(TradeState.State.valueOf(status));
			   repository.save(trade);
			   return new ResponseEntity<>("Trade status updated successfully", HttpStatus.OK);
		   }
		   else
		   {
			   return new ResponseEntity<>("Invalid operation", HttpStatus.FORBIDDEN);
		   }
	   }
	   else {
		   return new ResponseEntity<>("Invalid operation", HttpStatus.FORBIDDEN);
	   }
//	   repository.save(trade);
//	   return trade;
	}
	@RequestMapping(value = "/tradeAdvice/{id}", method = RequestMethod.GET)
	public String getTradeAdvice(@PathVariable("id") ObjectId id) throws IOException {
		Trade trade = repository.findBy_id(id);
		String tradeticker = trade.getTicker();
		Calendar from = Calendar.getInstance();
		Calendar to = Calendar.getInstance();
		from.add(Calendar.WEEK_OF_MONTH, -1);
		
		Stock ticker = YahooFinance.get(tradeticker);
		List<HistoricalQuote> tickerHistQuotes = ticker.getHistory(from, to, Interval.DAILY);
		double previous_value = tickerHistQuotes.get(0).getClose().doubleValue();
		double current_value = tickerHistQuotes.get(tickerHistQuotes.size()-1).getClose().doubleValue();
		double current_value_1 = tickerHistQuotes.get(tickerHistQuotes.size()-2).getClose().doubleValue();
		
		double sum = 0.0;
		for(int j=0;j<tickerHistQuotes.size(); j++) { 
			sum = sum+tickerHistQuotes.get(j).getClose().doubleValue();
		}	
		sum = sum/tickerHistQuotes.size();
		
		double no0_1 = current_value * 0.001;
		double current_value0_1_high = current_value + no0_1;
		double current_value0_1_less = current_value - no0_1;
		
		if(sum >= current_value0_1_less && sum <=current_value0_1_high) {
			return "SELL";
		}
		else if((current_value > current_value_1) && (sum > previous_value)) {
			return "BUY";
		}
		else  {
			return "HOLD";
		}
	}
}
